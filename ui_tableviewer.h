/********************************************************************************
** Form generated from reading UI file 'tableviewer.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TABLEVIEWER_H
#define UI_TABLEVIEWER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TableViewer
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout_3;
    QSplitter *splitter;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLineEdit *filterLine;
    QListWidget *listWidget;
    QProgressBar *loadingBar;
    QPushButton *refreshButton;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout;
    QTableWidget *tableWidget;
    QLabel *orgLabel;
    QWidget *tab_2;
    QGridLayout *gridLayout_2;
    QFrame *barFrame;
    QHBoxLayout *horizontalLayout;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *TableViewer)
    {
        if (TableViewer->objectName().isEmpty())
            TableViewer->setObjectName(QString::fromUtf8("TableViewer"));
        TableViewer->resize(1224, 692);
        centralwidget = new QWidget(TableViewer);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout_3 = new QGridLayout(centralwidget);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        splitter = new QSplitter(centralwidget);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        layoutWidget = new QWidget(splitter);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        filterLine = new QLineEdit(layoutWidget);
        filterLine->setObjectName(QString::fromUtf8("filterLine"));

        verticalLayout->addWidget(filterLine);

        listWidget = new QListWidget(layoutWidget);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));

        verticalLayout->addWidget(listWidget);

        loadingBar = new QProgressBar(layoutWidget);
        loadingBar->setObjectName(QString::fromUtf8("loadingBar"));
        loadingBar->setValue(100);
        loadingBar->setTextVisible(false);

        verticalLayout->addWidget(loadingBar);

        refreshButton = new QPushButton(layoutWidget);
        refreshButton->setObjectName(QString::fromUtf8("refreshButton"));

        verticalLayout->addWidget(refreshButton);

        splitter->addWidget(layoutWidget);
        tabWidget = new QTabWidget(splitter);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        gridLayout = new QGridLayout(tab);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        tableWidget = new QTableWidget(tab);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));

        gridLayout->addWidget(tableWidget, 1, 0, 1, 1);

        orgLabel = new QLabel(tab);
        orgLabel->setObjectName(QString::fromUtf8("orgLabel"));

        gridLayout->addWidget(orgLabel, 0, 0, 1, 1);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        gridLayout_2 = new QGridLayout(tab_2);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        barFrame = new QFrame(tab_2);
        barFrame->setObjectName(QString::fromUtf8("barFrame"));
        horizontalLayout = new QHBoxLayout(barFrame);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));

        gridLayout_2->addWidget(barFrame, 0, 0, 1, 1);

        tabWidget->addTab(tab_2, QString());
        splitter->addWidget(tabWidget);

        gridLayout_3->addWidget(splitter, 0, 0, 1, 1);

        TableViewer->setCentralWidget(centralwidget);
        menubar = new QMenuBar(TableViewer);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1224, 22));
        TableViewer->setMenuBar(menubar);
        statusbar = new QStatusBar(TableViewer);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        TableViewer->setStatusBar(statusbar);

        retranslateUi(TableViewer);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(TableViewer);
    } // setupUi

    void retranslateUi(QMainWindow *TableViewer)
    {
        TableViewer->setWindowTitle(QApplication::translate("TableViewer", "TableViewer", nullptr));
        label->setText(QApplication::translate("TableViewer", "Search Filter", nullptr));
        refreshButton->setText(QApplication::translate("TableViewer", "Refresh Network Request", nullptr));
        orgLabel->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("TableViewer", "Tab 1", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("TableViewer", "Tab 2", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TableViewer: public Ui_TableViewer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TABLEVIEWER_H
