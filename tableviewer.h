#ifndef TABLEVIEWER_H
#define TABLEVIEWER_H

#include "graphic.h"
#include<jsonparsing.h>
#include "networkhelper.h"
#include <QMainWindow>
#include<networkhelper.h>
#include<QListWidgetItem>
#include<QListWidget>
#include<QVector>
#include<QTableWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class TableViewer; }
QT_END_NAMESPACE

class TableViewer : public QMainWindow
{
	Q_OBJECT

public:
	TableViewer(QWidget *parent = nullptr);
	~TableViewer();
public slots:
	void vectorSendGraphic();
	void createListWidget();
	void vectEndpoints();

signals:
	void tableFinish(QVector<JsonParsing::MyTableStruct>vectorSignal);

private slots:
	void on_listWidget_itemClicked(QListWidgetItem *item);
	void on_filterLine_editingFinished();
	void orgInfoShow();
	void parsCalled();
	void on_refreshButton_clicked();

private:
	int counter;
	int counterMang;
	QTableWidgetItem *tableItem;
	QString file;
	QVector<JsonParsing::MyTableStruct>endpVector;
	void TableWidgetDisplay(QVector<JsonParsing::MyTableStruct>MyTable);
	void listWidget(QStringList lstwdgtStr);
	void drawTable(QString target);
	void loadingBar();
	Ui::TableViewer *ui;
	NetworkHelper *helper= nullptr;
	JsonParsing *parser = nullptr;
	Graphic *grphc;
};

#endif // TABLEVIEWER_H
