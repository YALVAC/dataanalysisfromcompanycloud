/****************************************************************************
** Meta object code from reading C++ file 'networkhelper.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.8)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "networkhelper.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'networkhelper.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.8. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_NetworkHelper_t {
    QByteArrayData data[19];
    char stringdata0[241];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_NetworkHelper_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_NetworkHelper_t qt_meta_stringdata_NetworkHelper = {
    {
QT_MOC_LITERAL(0, 0, 13), // "NetworkHelper"
QT_MOC_LITERAL(1, 14, 9), // "orgFinish"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 7), // "orgData"
QT_MOC_LITERAL(4, 33, 9), // "strFinish"
QT_MOC_LITERAL(5, 43, 11), // "allLogsData"
QT_MOC_LITERAL(6, 55, 12), // "fromApiFinis"
QT_MOC_LITERAL(7, 68, 9), // "endpoints"
QT_MOC_LITERAL(8, 78, 17), // "getByFileFinished"
QT_MOC_LITERAL(9, 96, 14), // "QNetworkReply*"
QT_MOC_LITERAL(10, 111, 11), // "byFileReply"
QT_MOC_LITERAL(11, 123, 21), // "getFileContentFromApi"
QT_MOC_LITERAL(12, 145, 13), // "clickFileName"
QT_MOC_LITERAL(13, 159, 18), // "getAllLogsFinished"
QT_MOC_LITERAL(14, 178, 5), // "reply"
QT_MOC_LITERAL(15, 184, 29), // "getFileContentFromAPIFinished"
QT_MOC_LITERAL(16, 214, 8), // "apiReply"
QT_MOC_LITERAL(17, 223, 9), // "getByFile"
QT_MOC_LITERAL(18, 233, 7) // "logSend"

    },
    "NetworkHelper\0orgFinish\0\0orgData\0"
    "strFinish\0allLogsData\0fromApiFinis\0"
    "endpoints\0getByFileFinished\0QNetworkReply*\0"
    "byFileReply\0getFileContentFromApi\0"
    "clickFileName\0getAllLogsFinished\0reply\0"
    "getFileContentFromAPIFinished\0apiReply\0"
    "getByFile\0logSend"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_NetworkHelper[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,
       4,    1,   57,    2, 0x06 /* Public */,
       6,    1,   60,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    1,   63,    2, 0x0a /* Public */,
      11,    1,   66,    2, 0x0a /* Public */,
      13,    1,   69,    2, 0x0a /* Public */,
      15,    1,   72,    2, 0x0a /* Public */,
      17,    1,   75,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void, QMetaType::QString,    7,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void, 0x80000000 | 9,   14,
    QMetaType::Void, 0x80000000 | 9,   16,
    QMetaType::Void, QMetaType::QString,   18,

       0        // eod
};

void NetworkHelper::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<NetworkHelper *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->orgFinish((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->strFinish((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->fromApiFinis((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->getByFileFinished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 4: _t->getFileContentFromApi((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->getAllLogsFinished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 6: _t->getFileContentFromAPIFinished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 7: _t->getByFile((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 3:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        case 5:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        case 6:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (NetworkHelper::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&NetworkHelper::orgFinish)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (NetworkHelper::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&NetworkHelper::strFinish)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (NetworkHelper::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&NetworkHelper::fromApiFinis)) {
                *result = 2;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject NetworkHelper::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_NetworkHelper.data,
    qt_meta_data_NetworkHelper,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *NetworkHelper::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *NetworkHelper::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_NetworkHelper.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int NetworkHelper::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void NetworkHelper::orgFinish(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void NetworkHelper::strFinish(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void NetworkHelper::fromApiFinis(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
