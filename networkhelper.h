#ifndef NETWORKHELPER_H
#define NETWORKHELPER_H

#include<jsonparsing.h>
#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QString>
#include <QByteArray>
#include <QDebug>

class NetworkHelper : public QObject
{
	Q_OBJECT
public:
	QString allLogsStr;
	QString orgInfo;
	QString endpointsStr;
	QNetworkAccessManager *manager;
	QNetworkAccessManager *apiManager;
	QNetworkAccessManager *orgManager;
	void getAllLogs();
	explicit NetworkHelper(QObject *parent = nullptr);

public slots:
	void getByFileFinished(QNetworkReply *byFileReply);
	void getFileContentFromApi(QString clickFileName);
	void getAllLogsFinished(QNetworkReply *reply);
	void getFileContentFromAPIFinished(QNetworkReply *apiReply);
	void getByFile(QString logSend);

signals:
	void orgFinish(QString orgData);
	void strFinish(QString allLogsData);
	void fromApiFinis(QString endpoints);

private:
	JsonParsing *parser = nullptr;

};

#endif // NETWORKHELPER_H
