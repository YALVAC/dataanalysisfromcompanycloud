#ifndef JSONPARSING_H
#define JSONPARSING_H

#include "graphic.h"
#include <QString>
#include <QObject>
#include <QDateTime>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QJsonValue>
#include <QMap>

class JsonParsing : public QObject
{
	Q_OBJECT

public:
	QString org;
	QStringList dateIpList;
	QStringList logSendList;
	QMap<QString,int> version;
	QMap<QString, int> versionOnline;
	QMap<QString, int> versionOffline;
	void recentLogIpList(QString allLogStr);

	struct logValue
	{
		qint64 ts;
		QString ip;
	};

	struct ColumnName
	{
		QString comV;
		QString ipV;
		QString nameV;
		QString osV;
		QString ncV;
		QString statusV;
		QString uuidV;
	};

	struct Mydlpclipboard {
		QString ip;
		QString uuid;
		QString version;
	};

	struct Mydlpfs {
		QString ip;
		QString uuid;
		QString version;
	};

	struct Mydlpmail {
		QString ip;
		QString uuid;
		QString version;
	};

	struct Mydlpprinter {
		QString ip;
		QString uuid;
		QString version;
	};

	struct Mydlpweb {
		QString ip;
		QString uuid;
		QString version;
	};
	struct MyTableStruct {
		ColumnName computer;
		ColumnName ip;
		Mydlpclipboard mydlpclipboard;
		Mydlpfs mydlpfs;
		Mydlpmail mydlpmail;
		Mydlpprinter mydlpprinter;
		Mydlpweb mydlpweb;
		ColumnName name;
		ColumnName nc_version;
		ColumnName os_version;
		ColumnName status;
		ColumnName uuid;
	} MyTable;
	MyTableStruct table;
	QVector<MyTableStruct> endpoints;
	QVector<MyTableStruct>tableInfoParser(QString forTableParsApiStr);
	explicit JsonParsing(QObject *parent = nullptr);
signals:
	void finishDateIpList(QStringList dateIpStrList);
public slots:
private:
	Graphic *grphc = nullptr;
};

#endif // JSONPARSING_H
