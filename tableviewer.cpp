﻿#include "tableviewer.h"
#include "ui_tableviewer.h"

TableViewer::TableViewer(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::TableViewer)
{
	parser = new JsonParsing;
	helper = new NetworkHelper;
	counter=0;
	counterMang =0;
	helper->getAllLogs();
	ui->setupUi(this);
	ui->tabWidget->setTabText(0,"TABLE");
	ui->tabWidget->setTabText(1,"BAR CHART");
	ui->loadingBar->hide();
	connect(helper,SIGNAL(strFinish(QString)),this,SLOT(parsCalled()));
	connect(helper,SIGNAL(fromApiFinis(QString)),this,SLOT(vectEndpoints()));
	connect(this,SIGNAL(tableFinish(QVector<JsonParsing::MyTableStruct>)),this,SLOT(vectorSendGraphic()));
	connect(helper,SIGNAL(orgFinish(QString)),this,SLOT(orgInfoShow()));
	connect(parser,SIGNAL(finishDateIpList(QStringList)),this,SLOT(createListWidget()));
	grphc = new Graphic(this,ui->barFrame);
}

void TableViewer::drawTable(QString target)
{
	if(target.isEmpty()){
		qDebug()<<"file name empty!!!!";
		return;
	}
	helper->getFileContentFromApi(target);
	helper->getByFile(target);
}

void TableViewer::loadingBar()
{
	QProgressBar *loadingBar = ui->loadingBar;
	loadingBar->show();
	loadingBar->setValue(0);
	loadingBar->setStyleSheet("QProgressBar:: chunk {    background-color: #2196F3, width: 10px; margin: 0.5px;}");
	loadingBar->setMinimum(0);
	loadingBar->setMaximum(0);
	loadingBar->setValue(100);
}
void TableViewer::vectorSendGraphic()
{
	grphc->series->clear();
	if(endpVector.size()==0)
		return;
	grphc->barGraphic(parser->version,parser->versionOnline,parser->versionOffline);
	return;
}

void TableViewer::createListWidget()
{
	for(int i=0; i<parser->dateIpList.size(); i++){
		auto item = new QListWidgetItem(parser->dateIpList.at(i));
		item->setToolTip(parser->logSendList.at(i));
		ui->listWidget->addItem(item);
	}
}

void TableViewer::vectEndpoints()
{
	endpVector = parser->tableInfoParser(helper->endpointsStr);
	TableWidgetDisplay(endpVector);
	emit tableFinish(endpVector);
}

void TableViewer::on_listWidget_itemClicked(QListWidgetItem *item)
{
	ui->orgLabel->clear();
	ui->tableWidget->clearContents();
	counterMang = counterMang +1;
	if(counterMang>1){
		helper->orgInfo.clear();
		delete helper->apiManager;
		delete helper->orgManager;
	}
	loadingBar();
	auto changeItemRow = ui->listWidget->row(item);
	file = parser->logSendList.at(changeItemRow);
	drawTable(file);
}

void TableViewer::TableWidgetDisplay(QVector<JsonParsing::MyTableStruct> MyTable)
{

	QTableWidget *table = ui->tableWidget;
	table->setColumnCount(12);
	table->setRowCount(MyTable.size());
	if(MyTable.size()==0)
		return;
	QStringList upLabels;
	upLabels
			<<"computer"
		   <<"ip"
		  <<"name"
		 <<"nc_version"
		<<"os_version"
	   <<"status"
	  <<"uuid"
	 <<"mydlpclipboard"
	<<"mydlpfs"
	<<"mydlpemail"
	<<"mydlpprinter"
	<<"mydlpweb";
	table->setHorizontalHeaderLabels(upLabels);
	ui->loadingBar->hide();
	for(int i=0; i<MyTable.size(); i++){
		ui->tableWidget->setSortingEnabled(false);
		table->setStyleSheet(
					"QTableWidget{"
					"background-color: #C0C0C0;"
					"selection-background-color: rgb(52, 101, 164);"
					"}");
		table->setSelectionMode(QAbstractItemView::SingleSelection);
		table->setSelectionBehavior(QAbstractItemView::SelectRows);

		auto ep = MyTable[i];
		{
			tableItem = new QTableWidgetItem;
			tableItem->setText(QString("%1").arg(ep.computer.comV));
			table->setItem(i, 0, tableItem);
		}

		{
			tableItem = new QTableWidgetItem;
			tableItem->setText(QString("%1").arg(ep.ip.ipV));
			table->setItem(i, 1, tableItem);
		}

		{
			tableItem = new QTableWidgetItem;
			tableItem->setText(QString("%1").arg(ep.name.nameV));
			table->setItem(i, 2, tableItem);
		}

		{
			tableItem = new QTableWidgetItem;
			tableItem->setText(QString("%1").arg(ep.nc_version.ncV));
			table->setItem(i, 3, tableItem);
		}

		{
			tableItem = new QTableWidgetItem;
			tableItem->setText(QString("%1").arg(ep.os_version.osV));
			table->setItem(i, 4, tableItem);
		}

		{
			tableItem = new QTableWidgetItem;
			tableItem->setText(QString("%1").arg(ep.status.statusV));
			if(ep.status.statusV.contains("ONLINE")){
				table->setItem(i,5,tableItem);
				tableItem->setBackground(QBrush(Qt::green));
			}
			else{
				table->setItem(i, 5, tableItem);
				tableItem->setBackground(QBrush(Qt::red));
			}
		}

		{
			tableItem = new QTableWidgetItem();
			tableItem->setText(QString("%1").arg(ep.uuid.uuidV));
			table->setItem(i, 6, tableItem);
		}

		{
			tableItem = new QTableWidgetItem;
			tableItem->setText(QString("Version: %1").arg(ep.mydlpclipboard.version));
			table->setItem(i, 7, tableItem);
		}

		{
			tableItem = new QTableWidgetItem;
			tableItem->setText(QString("Version: %1").arg(ep.mydlpfs.version));
			table->setItem(i, 8, tableItem);
		}

		{
			tableItem = new QTableWidgetItem;
			tableItem->setText(QString("Version: %1").arg(ep.mydlpmail.version));
			table->setItem(i, 9, tableItem);

		}

		{
			tableItem = new QTableWidgetItem;
			tableItem->setText(QString("Version: %1").arg(ep.mydlpprinter.version));
			table->setItem(i, 10, tableItem);
		}

		{
			tableItem = new QTableWidgetItem;
			tableItem->setText(QString("Version: %1").arg(ep.mydlpweb.version));
			table->setItem(i, 11, tableItem);
		}
	}
	ui->tableWidget->setSortingEnabled(true);
	ui->loadingBar->hide();
	MyTable.clear();


}

void TableViewer::on_filterLine_editingFinished()
{
	QTableWidget *table = ui->tableWidget;
	QString Filter = ui->filterLine->text();
	for(int i = 0; i<table->rowCount(); i++){
		bool match = true;
		for(int j= 0; j<table->columnCount(); j++){
			QTableWidgetItem *item = table->item(i,j);
			if(item->text().contains(Filter)){
				match = false;
				break;
			}
		}
		table->setRowHidden(i,match);
	}
}

void TableViewer::orgInfoShow()
{
	ui->orgLabel->setText(helper->orgInfo);
}

void TableViewer::parsCalled()
{
	parser->recentLogIpList(helper->allLogsStr);
	return;
}


void TableViewer::on_refreshButton_clicked()
{
	counterMang =0;
	delete helper->manager;
	if(!helper->endpointsStr.isEmpty()) {
		helper->endpointsStr.clear();
		delete helper->apiManager;
		delete helper->orgManager;
	}
	ui->tableWidget->clear();
	ui->orgLabel->clear();
	ui->listWidget->clear();
	helper->getAllLogs();
}

TableViewer::~TableViewer()
{
	delete ui;
	delete parser;
	delete helper;
	delete grphc;
}
