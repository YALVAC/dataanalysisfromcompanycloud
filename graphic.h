#ifndef Graphic_H
#define Graphic_H

#include <QObject>
#include<QBarSeries>
#include<QBarSet>
#include <QColor>
#include <QBarCategoryAxis>
#include <QValueAxis>
#include<QChartView>
#include<QChart>

class Graphic : public QObject
{
	Q_OBJECT
public:
	Graphic(QObject *parent,QWidget *bar);
	QtCharts::QChart *chartBar;
	QtCharts::QBarSeries *series;
	QtCharts::QBarSet *seti0;
	QtCharts::QBarCategoryAxis *axisx;
	QtCharts::QValueAxis *axisY;
	QtCharts::QChartView *chartViewBar;
	void barGraphic(QMap<QString,int>barVers,QMap<QString,int>barVersOn,QMap<QString,int>barVersOff);
signals:
private:
	QWidget *uiBar;
};

#endif // Graphic_H
