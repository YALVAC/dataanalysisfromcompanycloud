#include "graphic.h"

using namespace QtCharts;
Graphic::Graphic(QObject *parent,QWidget *bar) : QObject(parent),uiBar(bar)
{
	series = new QBarSeries();
	chartBar = new QChart();
	axisx= new QBarCategoryAxis();
	axisY = new QValueAxis();
	chartViewBar = new QChartView(chartBar);
	chartBar->addSeries(series);
	QStringList categories;
	categories<<"ONLINE"<<"OFFLINE";
	axisx->append(categories);
	chartBar->addAxis(axisx,Qt::AlignBottom);
	series->attachAxis(axisx);
	chartBar->addAxis(axisY,Qt::AlignLeft);
	series->attachAxis(axisY);

}

void Graphic::barGraphic(QMap<QString, int> barVers, QMap<QString, int> barVersOn, QMap<QString, int> barVersOff)
{
	int val = 0;
	for(int i = 0; i<barVers.size(); i++){
		if(barVersOn.values().at(i) >= val)
			val = barVersOn.values().at(i);
		if(barVersOff.values().at(i) >= val)
			val = barVersOff.values().at(i);
	}
	for(int i=0; i<barVers.size(); i++) {
		seti0 = new QBarSet(barVers.keys().at(i));
		*seti0<<barVersOn.values().at(i)<<barVersOff.values().at(i);
		series->append(seti0);
	}
	chartBar->setTitle("ONLINE / OFFLINE BAR Graphic");
	chartBar->setAnimationOptions(QChart::SeriesAnimations);
	series->setLabelsVisible(true);
	chartBar->legend()->setVisible(true);
	axisY->setRange(0,val+10);
	chartBar->legend()->setAlignment(Qt::AlignBottom);
	chartViewBar->setRenderHint(QPainter::Antialiasing);
	chartViewBar->setParent(uiBar);
	return;
}
