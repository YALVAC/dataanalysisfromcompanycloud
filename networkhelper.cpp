#include "networkhelper.h"


NetworkHelper::NetworkHelper(QObject *parent) : QObject(parent)
{
}

void NetworkHelper::getByFileFinished(QNetworkReply *byFileReply)
{
	orgInfo.clear();
	QByteArray response = byFileReply->readAll();
	if(response.isEmpty())
		return;
	if (byFileReply->error()) {
		qDebug()<< "ERROR";
		qDebug()<< byFileReply->errorString();
	}
	else{
		QString orgResponse = QString::fromUtf8(response);
		orgResponse.replace("\"","");
		orgResponse.replace("\\u0022","\"");
		QJsonDocument doc = QJsonDocument::fromJson(orgResponse.toUtf8());
		QJsonObject json = doc.object();
		if(json.contains("org"))
			orgInfo = "ORG : "+json.value("org").toString();
	}


	emit orgFinish(orgInfo);
	return;
}

void NetworkHelper::getAllLogs()
{
	manager = new QNetworkAccessManager(this);
	QObject::connect(manager, SIGNAL(finished(QNetworkReply *)), this, SLOT(getAllLogsFinished(QNetworkReply *)));
	manager->get(QNetworkRequest(QUrl("https://mydlp-log.sparsetechnology.com/LogsView")));
}

void NetworkHelper::getFileContentFromApi(QString clickFileName)
{
	apiManager = new QNetworkAccessManager(this);
	QObject::connect(apiManager, SIGNAL(finished(QNetworkReply *)), this,SLOT(getFileContentFromAPIFinished(QNetworkReply *)));
	auto url = "https://mydlp-log.sparsetechnology.com/LogsView/GetRawDataByFile/" + clickFileName;
	apiManager->get(QNetworkRequest(QUrl(url)));
}

void NetworkHelper::getByFile(QString logSend)
{
	orgManager = new QNetworkAccessManager(this);
	QObject::connect(orgManager, SIGNAL(finished(QNetworkReply*)), this,SLOT(getByFileFinished(QNetworkReply*)));
	auto orgUrl = "https://mydlp-log.sparsetechnology.com/LogsView/GetByFile/" + logSend;
	orgManager->get(QNetworkRequest(QUrl(orgUrl)));
}

void NetworkHelper::getAllLogsFinished(QNetworkReply *reply)
{
	QByteArray response = reply->readAll();
	if (response.isEmpty())
		return;
	if (reply->error()) {
		qDebug() << "ERROR!";
		qDebug() << reply->errorString();
	}
	allLogsStr = QString::fromUtf8(response);
	emit strFinish(allLogsStr);
	return;
}

void NetworkHelper::getFileContentFromAPIFinished(QNetworkReply *apiReply)
{
	endpointsStr.clear();
	QByteArray response = apiReply->readAll();
	if(response.isEmpty())
		return;
	endpointsStr = QString::fromUtf8(response);
	endpointsStr.replace(QRegExp("\""),"");
	endpointsStr.replace(QRegExp("\\\\u0022"),"\"");
	endpointsStr.replace(QRegExp("\\\\"),"/");
	endpointsStr.replace(QRegExp("////"),"/");
	emit fromApiFinis(endpointsStr);
	return;
}
