#include "jsonparsing.h"

JsonParsing::JsonParsing(QObject  *parent) : QObject(parent)
{

}

QVector<JsonParsing::MyTableStruct> JsonParsing::tableInfoParser(QString forTableParsApiStr)
{
	version.clear();
	versionOffline.clear();
	versionOnline.clear();
	endpoints.clear();
	QJsonDocument doc = QJsonDocument::fromJson(forTableParsApiStr.toUtf8());
	QJsonObject json = doc.object();
	QJsonValue endpointsValue = json.value("endpoints");
	if (endpointsValue.type() == QJsonValue::Array) {
		QJsonArray endpointsArray = endpointsValue.toArray();
		for (QJsonValue arr: endpointsArray) {
			QString forMap;
			QJsonObject obj = arr.toObject();
			if (obj.contains("computer")){
				ColumnName computerValue;
				computerValue.comV = obj.value("computer").toString();
				table.computer = computerValue;
			}

			if (obj.contains("ip")) {
				ColumnName ipValue;
				ipValue.ipV = obj.value("ip").toString();
				table.ip = ipValue;

			}

			if (obj.contains("name")) {
				ColumnName nameValue;
				nameValue.nameV = obj.value("name").toString();
				table.name = nameValue;
			}

			if (obj.contains("nc_version")) {
				forMap = obj.value("nc_version").toString();
				ColumnName ncValue;
				ncValue.ncV=forMap;
				versionOnline[forMap];
				versionOffline[forMap];
				table.nc_version = ncValue;
				if (!version.contains(forMap))
					version[forMap] = 0;
				version[forMap] = version[forMap] + 1;
			}
			if (obj.contains("os_version")){
				ColumnName osValue;
				osValue.osV = obj.value("os_version").toString();
				table.os_version = osValue;
			}

			if (obj.contains("status")){
				bool statusBool = obj.value("status").toBool();
				table.status.statusV = statusBool ? "ONLINE" : "Offline";

				if(statusBool == true){
					versionOnline[forMap] = versionOnline[forMap] + 1;
				}
				else
					versionOffline[forMap] = versionOffline[forMap] +1;
			}

			if (obj.contains("uuid")){
				ColumnName uuidValue;
				uuidValue.uuidV = obj.value("uuid").toString();
				table.uuid = uuidValue;
				uuidValue.uuidV.clear();
			}

			if (obj.contains("mydlpclipboard")){
				QJsonObject dlpclipboardObj = obj.value("mydlpclipboard").toObject();
				Mydlpclipboard mydlpValue;
				mydlpValue.version = dlpclipboardObj.value("version").toString();
				table.mydlpclipboard = mydlpValue;
			}

			if (obj.contains("mydlpfs")){
				QJsonObject dlpfsObj = obj.value("mydlpfs").toObject();
				Mydlpfs mydlpValue;
				mydlpValue.version = dlpfsObj.value("version").toString();
				table.mydlpfs = mydlpValue;
			}

			if (obj.contains("mydlpmail")){
				QJsonObject dlpmailObj = obj.value("mydlpmail").toObject();
				Mydlpmail mydlpValue;
				mydlpValue.version = dlpmailObj.value("version").toString();
				table.mydlpmail = mydlpValue;
			}

			if (obj.contains("mydlpprinter")){
				QJsonObject dlpprinterObj = obj.value("mydlpprinter").toObject();
				Mydlpprinter mydlpValue;
				mydlpValue.version = dlpprinterObj.value("version").toString();
				table.mydlpprinter = mydlpValue;
			}

			if (obj.contains("mydlpweb")){
				QJsonObject dlpwebObj = obj.value("mydlpweb").toObject();
				Mydlpweb mydlpValue;
				mydlpValue.version = dlpwebObj.value("version").toString();
				table.mydlpweb = mydlpValue;
			}
			endpoints.push_back(table);
		}
	}
	return endpoints;
}

void JsonParsing::recentLogIpList(QString allLogStr)
{
	dateIpList.clear();
	logSendList.clear();
	QString logSendStr;
	QJsonDocument jsonDoc = QJsonDocument::fromJson(allLogStr.toUtf8());
	QJsonArray array = jsonDoc.array();
	QMap<QString, QMap<qint64, QString>> logs;
	foreach (const QJsonValue &value, array) {
		QString logFile = value.toString();
		logFile.replace(".log", "");
		logFile.replace("ffff", "");
		QStringList stringList = logFile.split(QRegExp("[_]"), QString::SkipEmptyParts);
		logValue lv;
		if (stringList.size() != 2)
			continue;
		lv.ip = stringList[0];
		lv.ts = stringList.at(1).toLongLong();
		logs[lv.ip].insert(lv.ts, value.toString());

	}
	QMapIterator<QString, QMap<qint64, QString>> i(logs);
	while (i.hasNext()) {
		i.next();
		auto ipSend= i.key();
		QMapIterator<qint64, QString> ii(i.value());
		while (ii.hasNext()) {
			ii.next();
			logSendStr = ii.value();
		}
		QString strDate = QDateTime::fromMSecsSinceEpoch(ii.key()).toString("dd.MM.yyyy  hh:mm:ss");
		strDate.insert(0, QString("["));
		strDate.insert(22, QString("]	"));
		QString dateIpStr= strDate + ipSend;
		logSendList.append(logSendStr);
		dateIpList.append(dateIpStr);
	}
	emit finishDateIpList(dateIpList);
}
