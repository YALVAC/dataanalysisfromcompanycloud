#include "tableviewer.h"

#include <QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	TableViewer w;
	w.show();
	return a.exec();
}
