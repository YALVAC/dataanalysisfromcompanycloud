/****************************************************************************
** Meta object code from reading C++ file 'tableviewer.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.8)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "tableviewer.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#include <QtCore/QVector>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'tableviewer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.8. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TableViewer_t {
    QByteArrayData data[15];
    char stringdata0[249];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TableViewer_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TableViewer_t qt_meta_stringdata_TableViewer = {
    {
QT_MOC_LITERAL(0, 0, 11), // "TableViewer"
QT_MOC_LITERAL(1, 12, 11), // "tableFinish"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 35), // "QVector<JsonParsing::MyTableS..."
QT_MOC_LITERAL(4, 61, 12), // "vectorSignal"
QT_MOC_LITERAL(5, 74, 17), // "vectorSendGraphic"
QT_MOC_LITERAL(6, 92, 16), // "createListWidget"
QT_MOC_LITERAL(7, 109, 13), // "vectEndpoints"
QT_MOC_LITERAL(8, 123, 25), // "on_listWidget_itemClicked"
QT_MOC_LITERAL(9, 149, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(10, 166, 4), // "item"
QT_MOC_LITERAL(11, 171, 29), // "on_filterLine_editingFinished"
QT_MOC_LITERAL(12, 201, 11), // "orgInfoShow"
QT_MOC_LITERAL(13, 213, 10), // "parsCalled"
QT_MOC_LITERAL(14, 224, 24) // "on_refreshButton_clicked"

    },
    "TableViewer\0tableFinish\0\0"
    "QVector<JsonParsing::MyTableStruct>\0"
    "vectorSignal\0vectorSendGraphic\0"
    "createListWidget\0vectEndpoints\0"
    "on_listWidget_itemClicked\0QListWidgetItem*\0"
    "item\0on_filterLine_editingFinished\0"
    "orgInfoShow\0parsCalled\0on_refreshButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TableViewer[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   59,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,   62,    2, 0x0a /* Public */,
       6,    0,   63,    2, 0x0a /* Public */,
       7,    0,   64,    2, 0x0a /* Public */,
       8,    1,   65,    2, 0x08 /* Private */,
      11,    0,   68,    2, 0x08 /* Private */,
      12,    0,   69,    2, 0x08 /* Private */,
      13,    0,   70,    2, 0x08 /* Private */,
      14,    0,   71,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 9,   10,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void TableViewer::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<TableViewer *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->tableFinish((*reinterpret_cast< QVector<JsonParsing::MyTableStruct>(*)>(_a[1]))); break;
        case 1: _t->vectorSendGraphic(); break;
        case 2: _t->createListWidget(); break;
        case 3: _t->vectEndpoints(); break;
        case 4: _t->on_listWidget_itemClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 5: _t->on_filterLine_editingFinished(); break;
        case 6: _t->orgInfoShow(); break;
        case 7: _t->parsCalled(); break;
        case 8: _t->on_refreshButton_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (TableViewer::*)(QVector<JsonParsing::MyTableStruct> );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&TableViewer::tableFinish)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TableViewer::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_TableViewer.data,
    qt_meta_data_TableViewer,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *TableViewer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TableViewer::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TableViewer.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int TableViewer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 9;
    }
    return _id;
}

// SIGNAL 0
void TableViewer::tableFinish(QVector<JsonParsing::MyTableStruct> _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
